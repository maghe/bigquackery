#pragma once
#ifndef __HASHTABLE_H__
#define __HASHTABLE_H__
#include "vec.h"
#include "llist.h"

#define MKHASHTABLE(NAME, K, T ) \
typedef struct NAME##_pair \
{ \
	K key; \
	uint64_t key_hash; \
	T data; \
} NAME##_pair; \
MKVEC(NAME, NAME##_pair ); \
\
void NAME##_init(NAME *hashtable) \
{ \
	hashtable->len = hashtable->maxlen = 0; \
	hashtable->data = malloc(sizeof(NAME##_pair)); \
} \
void NAME##_add(NAME *hashtable, K *key, T* data ) \
{ \
	if( hashtable->len+1 > ( 1 << hashtable->maxlen ) ) \
	{ \
		hashtable->maxlen <<= 1; \
		hashtable->data = realloc(hashtable->data, hashtable->maxlen ); \
		for \
	} \


#endif
