#pragma once
#ifndef __LLIST_H__
#define __LLIST_H__
#include <stddef.h>

#define MKLLIST(NAME, T ) \
typedef struct NAME \
{ \
	T data; \
	struct NAME *next; \
} NAME;

#define MKDLLIST(NAME, T ) \
typedef struct NAME \
{ \
	T data; \
	struct NAME *next, *prev; \
} NAME;

#endif
