#pragma once
#ifndef __BTREE_H__
#define __BTREE_H__
#include <stddef.h>

#define MKBTREE(NAME, T ) \
typedef struct NAME \
{ \
	T data; \
	struct NAME *lr[2]; \
} NAME;

#endif
