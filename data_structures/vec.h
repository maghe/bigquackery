#pragma once
#ifndef __VEC_H__
#define __VEC_H__
#include <stddef.h>

#define MKVEC(NAME, T ) \
typedef struct \
{ \
	T *data; \
	size_t len; \
	size_t maxlen; \
} NAME;

#define VEC_BACK(v) v.data[v.len-1]
#endif
