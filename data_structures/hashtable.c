#include <stdio.h>
#include "vec.h"
#include "llist.h"
#include "inttypes.h"
#include <stdlib.h>

extern uint64_t huffhash(char *);

typedef struct hashtable_pair
{
	uint64_t key_hash;
	char *key;
	int data;
} hashtable_pair;

MKDLLIST(hashtable_llist, hashtable_pair )
MKVEC(hashtable, hashtable_llist* );

void hashtable_init(hashtable *hasht)
{
	hasht->len = hasht->maxlen = 0;
	hasht->data = NULL;
}

void hashtable_add(hashtable *hasht, char *key, int data )
{

	if( hasht->len+1 > hasht->maxlen )
	{
		if( hasht->maxlen == 0 )
		{
			hasht->data = (hashtable_llist **)malloc(sizeof(hashtable_llist *));
			hasht->data[0] = NULL;
			hasht->maxlen = 1;
		}
		else
		{
			hasht->maxlen <<= 1;
			hasht->data = (hashtable_llist **)realloc(hasht->data, hasht->maxlen );
			for(size_t i=hasht->maxlen>>1; i < hasht->maxlen; i++ )
				hasht->data[i] = NULL;
			for(size_t i=0; i < (hasht->maxlen>>1); i++ )
			{
				for(hashtable_llist **head=hasht->data+i; *head != NULL; )
				{
					if( head[0]->data.key_hash & (hasht->maxlen>>1) )
					{

						//remove the element on the head
						hashtable_llist *oldhead = head[0];
						if( head[0]->next != NULL )
							head[0]->next->prev = head[0]->prev;
						*head = head[0]->next;

						hashtable_llist **newhead = hasht->data+(oldhead->data.key_hash & (hasht->maxlen-1));
						hashtable_llist *next = *head == NULL ? NULL : *head;

						newhead[0] = oldhead;
						newhead[0]->prev = NULL;
						newhead[0]->next = next;

					}
					else
						head = head = &(head[0]->next);
				}
			}
		}
	}
	
	uint64_t key_hash = huffhash(key);
	hashtable_llist **head = hasht->data+(key_hash & (hasht->maxlen-1));
	hashtable_llist *next = *head == NULL ? NULL : *head;

	head[0] = (hashtable_llist *)malloc(sizeof(hashtable_llist));
	head[0]->data = (hashtable_pair){key_hash, key, data};
	head[0]->prev = NULL;
	head[0]->next = next;

	hasht->len++;

}

void hashtable_destroy(hashtable *hasht)
{
	
}

int main()
{
	hashtable hasht;
	hashtable_init(&hasht);	
	hashtable_add(&hasht, "hello", 0 );
	hashtable_add(&hasht, "boot", 1 );
	hashtable_add(&hasht, "bye", 2 );
	return 0;
}
