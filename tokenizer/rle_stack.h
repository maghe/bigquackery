#ifndef __RLE_STACK_H__
#define __RLE_STACK_H__

#include "../data_structures/vec.h"
#include <stdio.h>
#include <string.h>

#ifdef PACK_RLE_CHAR_CELL
#define CELL_PACKING __attribute_((packed))
#else
#define CELL_PACKING
#endif

typedef struct rle_stack_cell
{
	unsigned char rep;
	int val;
} rle_stack_cell CELL_PACKING;

MKVEC(rle_stack, rle_stack_cell )

int rle_stack_push(rle_stack *stack, int val );

typedef struct
{
	unsigned char ok;
	int val;
} CELL_PACKING maybe_int;

maybe_int rle_stack_pop(rle_stack *stack);
maybe_int rle_stack_back(rle_stack *stack);

#endif

