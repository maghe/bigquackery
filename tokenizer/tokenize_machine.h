#pragma once
#ifndef _TOKENIZE_MACHINE_H__

#include "rle_stack.h"

typedef struct
{
	unsigned char low, high;
	int next;
} tokenize_transition;

MKVEC(tokenize_state, tokenize_transition )
MKVEC(tokenize_table, tokenize_state )
MKVEC(vec_char, unsigned char )

#endif
