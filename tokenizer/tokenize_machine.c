#include "tokenize_machine.h"

int tokenize(tokenize_table *table)
{

	unsigned char input_vec[1024] = {0};
	vec_char input = {input_vec, 1, 1024};
	rle_stack state_stack = (rle_stack){(rle_stack_cell[1024]){}, 0, 1024 };
	rle_stack pos_stack = (rle_stack){(rle_stack_cell[1024]){}, 0, 1024 };
	int max_match_state = 0;
	size_t curr_idx=0, max_match_len = 0;
	size_t pos = 0;
	int state = 0;
	maybe_int maybe_state;

	while(1)
	{
		//TODO: handle 1023 chars already inserted!!
		//TODO: handle EOF properly!!!
		if( curr_idx+1 == input.len )
		{
			input.data[curr_idx] = getchar();
			input.data[input.len] = 0;
			input.len++;
		}
		for(size_t i=pos; i < table->data[state].len; i++ )
		{
			if(
				table->data[state].data[i].low <= input.data[curr_idx] &&
				input.data[curr_idx] <= table->data[state].data[i].high
			)
			{
				int next = table->data[state].data[i].next;
				if( next < 0 ) 
				{
					if ( curr_idx+1 > max_match_len )
					{
						max_match_len = curr_idx+1;
						max_match_state = -next;
						printf("match: %d, curr_idx: %lu, state_stack.len: %lu\n", max_match_state, curr_idx, state_stack.len );
					}
					break;
				}
				else
				{
					rle_stack_push(&pos_stack, i+1 );
					rle_stack_push(&state_stack, state );
					pos = 0;
					state = table->data[state].data[i].next;
					curr_idx++;
					goto no_pop;
				}
			}
		}
		maybe_state = rle_stack_pop(&state_stack);
		if(!maybe_state.ok)
			break;
		state = maybe_state.val;
		pos = rle_stack_pop(&pos_stack).val;
		curr_idx--;
		no_pop: ;
	}

	return max_match_state;

}

#ifdef SIMPLE_UNIT_TEST

tokenize_table tab =
{
	.data = (tokenize_state[])
	{
		{ // state 0
			(tokenize_transition[])
			{ {'0', '9', 1 }, {'a', 'z', 2 }, { '"', '"', 4 } },
			3, 3
		},
		{ //state 1
			(tokenize_transition[])
			{ {'0', '9', 1 }, {'a', 'z', 2 }, { '.', '.', 3 }, {0, 255, -1 } },
			4, 4
		},
		{ //state 2
			(tokenize_transition[])
			{ {'0', '9', 2 }, {'a', 'z', 2 }, {0, 255, -2 } },
			3, 3
		},
		{ //added new state as state 3 
			(tokenize_transition[])
			{ {'0', '9', 3 }, {0, 255, -3 } },
			2, 2
		},
		{ //state 4
			(tokenize_transition[])
			{ {'\\', '\\', 5 }, { '"', '"', -4 }, { 0, 255, 4 } },
			3, 3
		},
		{ //state 5
			(tokenize_transition[])
			{ { 0, 255, 4 } },
			1, 1
		}

	},
	.len = 3,
	.maxlen = 3
};

char *token_names[5] = { "invalid token", "int", "ident", "float", "string" };

int main()
{
	int token = tokenize(&tab);
	printf("token_num: %d\ttoken_name: %s\n", token, token_names[token] );
	return 0;
}

#endif
