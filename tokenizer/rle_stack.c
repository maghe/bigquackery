#include "rle_stack.h"

int rle_stack_push(rle_stack *stack, int val )
{
	if( stack->len == stack->maxlen )
		return 0;
	else if(stack->len == 0)
	{
		stack->data->rep = 0;
		stack->data->val = val;
		stack->len++;
	}
	else
	{
		if( stack->data[stack->len-1].val == val && stack->data[stack->len-1].rep != 255 )
		{
			stack->data[stack->len-1].rep++;
		}
		else
		{
			stack->data[stack->len].rep = 0;
			stack->data[stack->len].val = val;
			stack->len++;
		}
	}
	return 1;
}

maybe_int rle_stack_pop(rle_stack *stack)
{
	if( stack->len == 0 )
		return (maybe_int){ 0, 0 };
	else if( stack->data[stack->len-1].rep == 0 )
	{
		return (maybe_int){ 1, stack->data[--(stack->len)].val };
	}
	else
	{
		stack->data[stack->len-1].rep--;
		return (maybe_int){ 1, stack->data[stack->len-1].val };
	}
}

#ifdef SIMPLE_UNIT_TEST
int main()
{
	rle_stack_cell cells[1024];
	rle_stack stack = (rle_stack){cells, 0, 1024 };
	memset(cells, 0, sizeof(cells) );
	push(&stack, 'O' );
	push(&stack, 'O' ); 
	push(&stack, 'L' );
	push(&stack, 'L' );
	push(&stack, 'E' );
	push(&stack, 'E' ); 
	push(&stack, 'H' );
	push(&stack, 'H' );
	for(maybe_int i; (i = pop(&stack)).ok; )
		putchar(i.val);
	return 0;
}
#endif
