;Galois field 2^127 multiplication utilities

section .text

global clmul_256

clmul_256:

	;as per X86_64 SYSV5 ABI rdi is the dest pointer(first param) and rdi is the source pointer(second param)

	movdqu xmm0, [rsi+0x00]
	movdqu xmm1, [rsi+0x10]

	movdqa xmm2, xmm0
	movdqa xmm3, xmm1

	pclmulqdq xmm2, xmm1, 0x00
	pclmulqdq xmm3, xmm0, 0xff
	
	movdqa xmm4, xmm0
	movdqa xmm5, xmm1
	pclmulqdq xmm4, xmm1, 0x10
	pclmulqdq xmm5, xmm0, 0x10
	pxor xmm4, xmm5
	movdqa xmm5, xmm4
	psrldq xmm4, 0x08
	pslldq xmm5, 0x08
	pxor xmm3, xmm4				;xmm3 contains the upper result of the product
	pxor xmm2, xmm5				;xmm2 contains the lower result of the product

	movdqu [rdi+0x00], xmm2
	movdqu [rdi+0x10], xmm3

ret	
