#include <stdio.h>
#include <time.h>
#include <stdint.h>

extern void clmul_256(uint64_t *dest, uint64_t *src );

int main()
{

	uint64_t src[4] = { 0x1, 0x2, 0x1, 0x4 };
	uint64_t dest[4];

	clmul_256(dest, src );

	printf("0x");
	for(int i=3; i>= 0; i-- )
	{
		printf("%016lx", dest[i] );
	}
	putchar('\n');

	return 0;

}
