#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "../data_structures/btree.h"
#include "../data_structures/llist.h"

uint16_t huffcodes_table[128][2];

struct charcount
{
	unsigned int weight;
	char c;
};

MKBTREE(huffman_tree, struct charcount );

/*
 * We are intrested in encoding C identifiers so
 * no need for malloc as I already know how much nodes I will need:
 */

struct huffman_tree  leaf_queue[63];
struct huffman_tree nodes_queue[62];

int nodes_queue_front = 0, nodes_queue_rear = 0;
int leaf_queue_front = 0;

int compare_nodes(const void *a, const void *b )
{ return ((huffman_tree *)a)->data.weight - ((huffman_tree *)b)->data.weight; }

void traverse_huffman_tree(huffman_tree *node, uint16_t huffman_code, uint16_t lenght )
{
	//is the node a leaf? if so store the Huffman Encoding into the translation array
	if(node->lr[0] == NULL)
	{
		huffcodes_table[node->data.c][0] = huffman_code;
		huffcodes_table[node->data.c][1] = lenght;
	}
	else
	{
		traverse_huffman_tree(node->lr[0], huffman_code << 1, lenght+1 );
		traverse_huffman_tree(node->lr[1], (huffman_code << 1)|1, lenght+1 );
	}
}

int main()
{

	//start populating the leaf queue: first with digits
	for(int i=0; i < 10; i++ )
	{
		//ascii hack: the digit i in ascii is '0'+i
		leaf_queue[i].data.c = '0'+i;
		/*limit hack: as the number of chars collected goes to inf
		 * the fact that we start from 1 doesn't matter much
		 */
		leaf_queue[i].data.weight = 1;
		leaf_queue[i].lr[0] = NULL;
		leaf_queue[i].lr[1] = NULL;
	}
	//Two more cycles, I know, but better safe than SIGSEGV
	for(int i=10; i < 36; i++ )
	{
		//You know what's going on now:
		leaf_queue[i].data.c = 'a'+i-10;
		leaf_queue[i].data.weight = 1;
		leaf_queue[i].lr[0] = NULL;
		leaf_queue[i].lr[1] = NULL;
	}
	for(int i=36; i < 62; i++ )
	{
		//You know what's going on now:
		leaf_queue[i].data.c = 'A'+i-36;
		leaf_queue[i].data.weight = 1;
		leaf_queue[i].lr[0] = NULL;
		leaf_queue[i].lr[1] = NULL;
	}
	leaf_queue[62].data.c = '_';
	leaf_queue[62].data.weight = 1;	
	leaf_queue[62].lr[0] = NULL;
	leaf_queue[62].lr[1] = NULL;


	for(int c=getchar(), nflag=0; c != -1; c = getchar() )
	{
		if( c == '_' )
		{
			leaf_queue[62].data.weight++;
			nflag = 1;
		}
		else if(('a' <= c) && (c <= 'z'))
		{
			leaf_queue[c-'a'+10].data.weight++;
			nflag = 1;
		}
		else if(('A' <= c) && (c <= 'Z'))
		{
			leaf_queue[c-'A'+36].data.weight++;
			nflag = 1;
		}
		else if(nflag && ('0' <= c) && (c <= '9'))
			leaf_queue[c-'0'].data.weight++;
	}

	qsort(leaf_queue, 63, sizeof(huffman_tree), compare_nodes );	

	//for(int i=0; i < 63; i++ )
	//	printf("//%c : %d\n", leaf_queue[i].data.c, leaf_queue[i].data.weight );

	//Loop when we Deque the two nodes with least probabilities:
	while( nodes_queue_front < 62 )
	{
		huffman_tree *lr[2];
		//first dequeue
		if( (leaf_queue_front < 63) && ( (nodes_queue_rear == 0) || (leaf_queue[leaf_queue_front].data.weight <= nodes_queue[nodes_queue_front].data.weight) ) )
		{
			lr[0] = leaf_queue+leaf_queue_front;
			leaf_queue_front++;
		}
		else
		{
			lr[0] = nodes_queue+nodes_queue_front;
			nodes_queue_front++;
		}
		//second dequeue
		if( (leaf_queue_front < 63) && ( (nodes_queue_rear == 0) || (leaf_queue[leaf_queue_front].data.weight <= nodes_queue[nodes_queue_front].data.weight) ) )
		{
			lr[1] = leaf_queue+leaf_queue_front;
			leaf_queue_front++;
		}
		else
		{
			lr[1] = nodes_queue+nodes_queue_front;
			nodes_queue_front++;
		}
		nodes_queue[nodes_queue_rear].data.weight = lr[0]->data.weight + lr[1]->data.weight;
		nodes_queue[nodes_queue_rear].lr[0] = lr[0];
		nodes_queue[nodes_queue_rear].lr[1] = lr[1];
		nodes_queue_rear++;
	}

	traverse_huffman_tree(nodes_queue+61, 0, 0 );

	//printf("-------------------------\n");
	printf("uint16_t huffcodes_table[127][2] =\n{\n");

	for(int i=0; i < 16; i++ )
	{
		putchar('\t');
		for(int j=0; j < 8; j++ )
				printf(
						"{0x%02x, 0x%02x }, ",
						huffcodes_table[(i<<3)+j][0],
						huffcodes_table[(i<<3)+j][1]
				);
		putchar('\n');
	}
	printf("};\n");

	return 0;

}
